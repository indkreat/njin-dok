/*================================================================

    N-ANI = Njas Animation 
    -creator = Anjas Gumbira
    -version 1.0
    -2021/02/04
================================================================
    ================================================================

    -REQUIRE-
    *njas.fx.css

    -FEATURE-
    *animation on scroll;
    *animation on hover;
    *smooth move to #target;
    ================================================================
================================================================*/
	$(window).on("scroll", function() {
		if($(window).scrollTop() > 40) {
			$('nav').addClass('fixed-top');
        }
        else {
            $('nav').removeClass('fixed-top');
        }
      });
	  
	  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });

  });
  
/* animation on scroll */
$(window).scroll(function() {
    $("[n-ani]").each(function(){//slideanim
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
            var t = $(this).attr('n-ani');
          $(this).addClass('n-'+t);
        }
    });

  });

/* animation on hover */
$(document).on('mouseenter', '[n-ani-hover]', function(event) {
         $('.n-ani-hover').removeClass('n-ani-hover')
        var t = $(this).attr('n-ani-hover');
        if(!$(this).hasClass('n-ani-hover')){
          $(this).addClass(t+' animated infinite n-ani-hover');
        }
 });
 $(document).on('mouseleave', '[n-ani-hover]', function(event) {
         
        var t = $(this).attr('n-ani-hover');
        $('.n-ani-hover').removeClass(t+' animated infinite n-ani-hover')
        
 })

 /* smooth move to #target */
 $(document).ready(function(){
  $("a").on('click', function(event) {
  if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
        window.location.hash = hash;
      });
    } // End if
  });
});