//==================================
/*==================================

    NJAS TOOLS

    VERSION : 1
    DATE : 2021/1/31
    OWNER: NJAS.G
    ngespot.blogspot.com
    ==================================

    Feature:
    -pageload(string) = untuk memberikan effect loading di halaman
    -note(title, html, function) = memberikan dialog dengan sweet alert
    -njasload(data) = meload data menggunakan ajax
    
    Required:
    -Jquery
    -Sweetalert2
    -Bootstrapt
    -FontAwesome

==================================
==================================*/
var ispload = false;
function pageload(b) {
    if (b == '' || b == null)
        b = 'Loading...';
    if (ispload)
        setpageload(false);
    else
        setpageload(true, b);
}

function setpageload(a, b) {
    if (b == '' || b == null)
        b = 'Loading...';
    if (!a) {
        $(".loading").remove();
        $('body').css('overflow', 'auto');
        ispload = false;
    } else if (!ispload) {
        $('body').prepend('<div class="loading"><div class="loading-njas"><i class="fa fa-cog fa-spin fa-3x fa-fw" style="font-size: 3vw;"></i><span class="" style="font-size: 3vw;font-weight: bolder;">' + b + '</span><div class="progress"><div class="progress-bar-animated progress-bar-striped bg-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div></div></div></div>');
        $('body').css('overflow', 'hidden');
        ispload = true;
    }
}

function note(t, b, f) {
    //t =title b=bodyhtml f=coderun
    if (t == '') t = 'Pesan';
    if (b == '') b = 'Hello Team :)';
    if (f == null) f = '';
    Swal.fire({
        title: t,
        html: b,
        showCancelButton: true,
        preConfirm: function() {
            eval(f)
        }
    });
    
    if(f=='none') $('.swal2-actions').hide();
}


function njasload( url, t, data, m) {
// var data = {page: page};
// njasload('blog.php', '#data-blog', page);
	if(m==null) m = 'POST'; 
	$.ajax({
        url: url,
        method: m,
        data: data,
        success: function(hasil) {
            $(t).html(hasil);
        }
    })
}

function najax(d, url, reload){
	if(reload == null) reload = true;
	if(url == null) url = "../ndata/nupdate.php";
	console.log(url+" <= url Update data => "+ d);
            	$.ajax({
					url: url,
					method: "POST",
					data: d,
					success: function(response) {
					  if(response == 'aman'){
					  	Swal.fire('Tersimpan!', 'Data yang dirubah sudah tersimpan di database.', 'success');
					  	if(reload) window.location.reload();
					  }else Swal.fire('Error!', 'Periksa kembali data yang dimasukan, atau hubungi tim pengembang!.', 'error');
						
					}
				});
}

