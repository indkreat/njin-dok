<?php
//========================================================
//  njin requirement data config
//========================================================
//
//  "host"       = database host;  
//  "db"         = database name;  
//  "db_user"    = database username;
//  "db_pass"    = database password;  
//  "home"       = homepage url location;  
//
//========================================================

    $njin['config'] = array(
    
        "host"      => "localhost",
        "db"        => "nblog",
        "db_user"   => "root",
        "db_pass"   => "",
        "home"      => "http://localhost/git/njin-dok/"
        
    );

//========================================================
//  optional config apps
//========================================================

    $njin['data'] = array(

        "app_name"   => "Njin Framework",
        "page_name"  => "Homepage",
        "app_logo"   => $njin["config"]["home"]."data/njin.jpg",
        "app_desc" => "New simply framework php",
        
    );
    
?>
