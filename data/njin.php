<?php
//=====================================================================
//  version     = 1.0.3
//  build date  = 2022/07/27 
//  Author      = Anjas G
//  website     = https://nge-trick.blogspot.com/
//=====================================================================
//  LIST FUNGSI
//=====================================================================
//  init():view  = inisiasi fungsi njin
//  index():view("home") = menampilkan data index page
//  draw():string = Menampilkan data dalam bentuk tabel html string
//  draw_array():string = Draw/menampilkan array ke dalam html text berdasarkan key
//  view():string = Memasukan data dan menampilkan dalam bentuk page
//  url(boelean):string = Get url homepage, menggunakan https atau tidak
//  cache():string = Get/Set cache data melalui key
//  ar_join():array = Menggabungkan 2 data array 
//  
//  file->upload($files:file, $tpath:string, $rename:bool):string =  upload file to spesifict folder
//  file->delete($path_file):string = hapus file berdasarkan path
//  db->get():array = Mengambil data array dari table
//  db-> get, set, rm, query
//  draw, ar_to_html, view
//
//=====================================================================
// 28/7 = update index controller, 
//=====================================================================

    //reqire congig.php
    include('./config.php');
    
    //set koneksi
    $conn = new mysqli($njin["config"]["host"], $njin["config"]["db_user"], $njin["config"]["db_pass"], $njin["config"]["db"]);
    if (!$conn) die("Connection failed: " . mysqli_connect_error());

    class Njin{
        public $file;
        public $db;
        
        function __construct(){
            $this->file = new Nfile();
            $this->db = new Ndb();
            session_start();
        }
        
        //main function of N-JIN
        function init(){
            global $njin, $n_jin;
            $url = $this->url(true);
            $cont = str_replace(strtolower($njin["config"]["home"]),"", strtolower($url));
            $roll = explode("?", $cont);
            $target = 'controller/'.$roll[0];
                if(file_exists($target)) return include($target);
                else return $this->view('404', null);
        }
        
        //main index
        function index(){
            global $njin, $n_jin, $data;
            return include('controller/index');
        }
        
        // draw data[] to table, return as html string
        function draw($data, $class, $action){
            $hasil ='';
            $haction ="";
            $vaction ="";
            if(!is_null($action)) {
                $haction = "<th>Action</th>";
                $vaction = "<td>$action</td>";
            }
            if(is_null($class)) $class = "w-100 table-responsive-sm table table-striped table-bordered dataTable no-footer";
            if (!is_null($data) && count($data) > 0){
                $hasil .= "<table class='njin-table $class'><thead><tr><th>".implode('</th><th>', array_keys(current($data)))."</th>$haction</tr></thead>
                    <tbody>";
                   
                foreach ($data as $row){
                    array_map('htmlentities', $row);
                    preg_match_all('~{{(.*?)}}~s', $vaction, $str);
                    foreach($str[1] as $value) {
                        $vaction = str_replace('{{'.$value.'}}', $row[$value], $vaction);
                    }
                    $hasil.= "<tr><td>" . implode('</td><td>', $row) . "</td>".$vaction."</tr>";
                    
                } 
                $hasil.= "</tbody></table>";
            }else $hasil = "<b>draw(DATA) IS NULL, PLEASE INSERT DATA ARRAY</b>";
            return $hasil;
        }
       // draw array to html string
        function draw_array($data, $html){
            $hasil = "";
             foreach ($data as $ar){
                $hasil .= $this->ar_set($ar, $html);
             }
             return $hasil;
        }
        function ar_set($ar, $html){
             preg_match_all('~{{(.*?)}}~s', $html, $str);
                foreach($str[1] as $value) {
                       $html = str_replace('{{'.$value.'}}', $ar[$value], $html);
                }
            return $html;
        }
       
       //view html from file
        function view($view, $data){
        global $njin;
            $h = file_get_contents('views/'.$view);
            $html = str_replace("{{%}}", $njin["config"]["home"], $h);
             if(is_null($data)) {
                 echo $html;
             }else {
                // echo njin_draw($html, $data);
                preg_match_all('~{{(.*?)}}~s', $html, $str);
                foreach($str[1] as $value) {
                        $sub = explode(".", $value);
                        if(count($sub)>1)
                        $html = str_replace('{{'.$value.'}}', $data[$sub[0]][$sub[1]], $html);
                        else $html = str_replace('{{'.$value.'}}', $data[$value], $html);
                }
                echo str_replace(array("{/{"),'{{',$html);
             }
        }
        
        //get url address use https or not ($protocol)
        function url($https){
            $domName = $_SERVER['HTTP_HOST'];
            $sourceURI = $_SERVER['REQUEST_URI'];
            if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
                $urlProtocol = "https";
            else
                $urlProtocol = "http";
            $urlAddress = $domName.$sourceURI;
            if($https == true)  $urlAddress = $urlProtocol."://".$domName.$sourceURI;
            
            return $urlAddress;
        }
        
        //get or set cache by key and value
        function cache($key, $val){
            global $_SESSION;
            if(is_null($val)){
                return $_SESSION[$key];
            }else {
                $_SESSION[$key] = $val;
            }
        }
        
        //join data dari 2 array
        function ar_join($join, $key, $val, $target){
            foreach ($join as $a) {
                if($a[$key] == $val)  return $a[$target];
            }
        }
        
    }
    
    class Nfile{
        public $title = "hallo";
        public $desc;
        function test(){
            echo $this->title;
        }
        
        //upload file to target folder
        function upload($files, $tpath, $rename){
            // print_r($files);
            if(is_null($rename)) $rename = true;
            $response = '';
            if(is_dir("$tpath")){
            }else if (!mkdir("$tpath")) { $response = "Failed to create $tpath"; }
                // else $response = "created";
                
                $data = '';
                $all = count($files['name']);
                if($all>0){
                    for($i=0; $i<$all; $i++){
                        
                        $target_path = $tpath;
                        $ext = explode('.', basename( $files['name'][$i]));
                         //$own = $ext[0].".".$ext[1];
                        $jname = date("Y-m-d\Thi-s").$i . "." . $ext[count($ext)-1]; 
                        
                        if($rename == true) $target_path = $target_path ."/". $jname;
                        else $target_path = $target_path ."/". basename( $files['name'][$i]);
            
                            if(move_uploaded_file($files['tmp_name'][$i], $target_path)) {
                                if($data=='') $data .= $target_path;
                                else $data .= ', '.$target_path;
                            } else {
                                if($data=='') $data .= 'fail';
                                else $data .= ', fail';
                            }
                    }
                }else $response = "no fileuploaded = $all"; 
                if($response=='') $response = $data;
            return $response;
            
        }
        
        //delete file in path, just file not folder
        function delete($file){
            if(is_dir("$file")){
                if(unlink($file)) $response = "success"; 
                else $response = "fail";
            }else $response = "file deleted";
            return $response;
        }
    }
    
    class Ndb{
        function get($table, $cond){
             global $conn;
             $sql = "SELECT * FROM $table WHERE $cond";
             if(is_null($cond)) $sql = "SELECT * FROM $table";
             $result = $conn->query($sql);
        
                 if ($result->num_rows > 0) {
                     	while ($row = mysqli_fetch_assoc($result)) {
                        $data[] = $row;
                     }
                 }else $data = 'nodata';
        
                //  return json_encode($data);
                 return $data;
        }
        //get data melalui query
        function get_query($query){
             global $conn;
             $result = $conn->query($query);
        
                 if ($result->num_rows > 0) {
                     	while ($row = mysqli_fetch_assoc($result)) {
                        $data[] = $row;
                     }
                 }else $data = 'nodata';
        
                //  return json_encode($data);
                 return $data;
        }
        function set($table, $id, $data){
            global $conn;
            $upsql = "UPDATE $table SET";
            $first = false;
            foreach($data as $key => $value) {
                if($first == false){ 
                    $first = true;
                    $upsql .=" $key = '$value'";
                }else $upsql .=", $key = '$value'";
            }
            
                 $response = -1;
                 $upsql .= " WHERE $id";
                 if (mysqli_query($conn, $upsql)) { 
                    $response = "success"; 
                 } else { $response = -1;}
                 return $response;
        }
        //remove data by condition
        function rm($table, $cond){
             global $conn;
             $response = 0;
             $sql = "DELETE FROM $table WHERE $cond";
            //  echo $sql;
             if (mysqli_query($conn, $sql)) $response = "success"; 
             else $response = -1;
             return $response;
        }
        function query($query, $rows){
             global $conn;
             if(is_null($rows)){
                 if (mysqli_query($conn, $query)) $data = "success"; 
                 else $data = 'fail';
             }else {
                 $results = mysqli_query($conn, $query);
    	 		 if (mysqli_num_rows($results) > 0) {
    			    while ($row = mysqli_fetch_assoc($results)) {
    			        foreach($rows as $value){
    			            $data[$value] = $row[$value];
    			        }
                     }
    			}else $data = 'nodata';
             }
             
			return $data;
        }
    }
    
    
    $n_jin = new Njin;
    
?>
